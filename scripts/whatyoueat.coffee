# Description:
#   Tell Hubot what you're eating today
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot i want to eat <anything> today- Set what you're eating today
#   hubot what is everyone eating today? - Find out what everyone want to eat
#
# Author:
#   ryuzaki01

module.exports = (robot) ->

  robot.respond /(?:what\'s|what is|whats) @?([\w .\-]+) eating(?:\?)$/i, (msg) ->
    name = msg.match[1].trim()

    if name is "you"
      msg.send "I dunno, robot things I guess."
    else if name.toLowerCase() is robot.name.toLowerCase()
      msg.send "i'm eating you!"
    else if name.match(/(everybody|everyone)/i)
      messageText = '';
      users = robot.brain.users()
      for k, u of users
          if u.eating
              messageText += "#{u.name} want to eat #{u.eating}\n"
          else
              messageText += ""
      if messageText.trim() is "" then messageText = "Nobody told me a thing."
      msg.send messageText
    else
      users = robot.brain.usersForFuzzyName(name)
      if users.length is 1
        user = users[0]
        user.eating = user.eating or [ ]
        if user.eating.length > 0
          msg.send "#{name} want to eat #{user.eating}."
        else
          msg.send "#{name} was not hungry."
      else if users.length > 1
        msg.send getAmbiguousUserText users
      else
        msg.send "#{name}? Who's that?"

  robot.respond /(?:i\'m|i am|im|i) (?:going to|gonna|want to|wanna) (?:eating|eat) (.*)/i, (msg) ->
    name = msg.message.user.name
    user = robot.brain.userForName name

    if typeof user is 'object'
      user.eating = msg.match[1]
      msg.send "Okay #{user.name}, got it."
    else if typeof user.length > 1
      msg.send "I found #{user.length} people named #{name}"
    else
      msg.send "I have never met #{name}"

